package domain;

public class UserApplication {
	private String username;
	private String password;
	private String email;
	private Boolean isPremium;
	private Boolean isAdmin;
	private Boolean accessProfile;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getIsPremium() {
		return isPremium;
	}
	public void setIsPremium(Boolean isPremium) {
		this.isPremium = isPremium;
	}
	public Boolean getIsAdmin() {
		return isAdmin;
	}
	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	public Boolean getAccessProfile() {
		return accessProfile;
	}
	public void setAccessProfile(Boolean accessProfile) {
		this.accessProfile = accessProfile;
	}

}
