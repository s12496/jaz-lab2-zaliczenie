package repositories;

import java.util.ArrayList;
import java.util.List;

import domain.UserApplication;

public class DummyUserApplicationRepository implements UserApplicationRepository{
	  
	 
	private static List<UserApplication> db = new ArrayList<UserApplication>();
	
	@Override
	public UserApplication getUserByEmailAddress (String email){
		for (UserApplication user: db){
			if (user.getEmail().equalsIgnoreCase(email))
				return user;
		}
		return null;
	}

	@Override
	public void add(UserApplication user){
		db.add(user);
	}
	
	@Override
	public int count(){
		return db.size();
	}

	@Override
	public List<UserApplication> ListOfUsers(){
		
		return db;
	}
	
	public static List<UserApplication> getMyList() {
		return db;
	}
}