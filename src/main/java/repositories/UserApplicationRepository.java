package repositories;

import java.util.List;

import domain.UserApplication;

public interface UserApplicationRepository {

	void add(UserApplication user);
	int count();
	UserApplication getUserByEmailAddress(String email);
	List<UserApplication> ListOfUsers();

}