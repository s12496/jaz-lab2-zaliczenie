package web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.UserApplication;
import repositories.DummyUserApplicationRepository;

@WebServlet("/managepermissions")
public class ManagePermissions extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
		}

public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,  IOException{
		
	HttpSession session = request.getSession();
	UserApplication user = retrieveApplicationFromRequest(request);
	List<UserApplication> repository = DummyUserApplicationRepository.getMyList();
	
	response.setContentType("text/html");
	
	for(int i = 0;i<repository.size();i++){
		if(user.getUsername().equals(repository.get(i).getUsername())){
			if(repository.get(i).getIsPremium()==true){
				repository.get(i).setIsPremium(false);
				response.getWriter().println("User removed from Premium access.");
		} else {
			repository.get(i).setIsPremium(true);
			response.getWriter().println("User added to Premium access.");
			}
			
		}
	}
}


private UserApplication retrieveApplicationFromRequest (HttpServletRequest request){
	UserApplication result = new UserApplication();
	result.setUsername(request.getParameter("username"));
	
	return result;
	
	}

}