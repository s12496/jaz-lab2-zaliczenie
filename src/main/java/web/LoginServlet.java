package web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.UserApplication;
import repositories.DummyUserApplicationRepository;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	

public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
	HttpSession session = request.getSession();
	String permission = "anonymous";
	session.setAttribute("permission", permission);
	
	List<UserApplication> repository = DummyUserApplicationRepository.getMyList();
	response.setContentType("text/html");
	boolean foundUser = false;
		
	for(int i = 0;i<repository.size();i++){
		if(repository.get(i).getUsername().equals(request.getParameter("username"))){
			permission = "regular";
		response.getWriter().println("User OK!<br/>");
		foundUser = true;
		}
		if(repository.get(i).getPassword().equals(request.getParameter("password"))){
			if(foundUser=true){
				response.sendRedirect("profile?username="+ repository.get(i).getUsername());
				response.getWriter().println("Password OK!<br/>Welcome " +repository.get(i).getUsername()+"!</br>");
			response.getWriter().println(
					"<a href='http://localhost:8080/profile'>Check Your profile page</a>");
		
			if(repository.get(i).getIsPremium()==true){
				permission = "premium";
			if(repository.get(i).getIsAdmin()==true){
				permission = "admin";
			}
			
			}
			
	}
	}else{
		response.getWriter().println("Password error!<br/><br/>");
		response.getWriter().println(
				"<a href='http://localhost:8080/login.jsp'>Try again</a>");
	}
}
	response.getWriter().println(session.getAttribute("permission"));
	session.setAttribute("permission", permission);
}

private UserApplication retriveUserFromRequest (HttpServletRequest request){
	UserApplication result = new UserApplication();
	result.setUsername(request.getParameter("username"));
	result.setEmail(request.getParameter("email"));
	result.setPassword(request.getParameter("password"));
	return result;
}
}