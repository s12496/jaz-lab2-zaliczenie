package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.UserApplication;
import repositories.DummyUserApplicationRepository;
import repositories.UserApplicationRepository;

@WebServlet("/userinit")
public class UserInit  extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	UserApplicationRepository repository;
	
	@Override 
	protected void doGet(HttpServletRequest request,HttpServletResponse response) 
			throws IOException,ServletException{
		this.doPost(request,response);
	    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
			
	repository = new DummyUserApplicationRepository();
	
	// Initial Regular user
	UserApplication regular= new UserApplication();
	regular.setUsername("user");
	regular.setPassword("user");
	regular.setEmail("user@creodron.pl");
	regular.setIsAdmin(false);
	regular.setIsPremium(false);
	regular.setAccessProfile(true);
	
	// Initial Premium user
	UserApplication premium = new UserApplication();
	premium.setUsername("premium");
	premium.setPassword("premium");
	premium.setEmail("premium@creodron.pl");
	premium.setIsAdmin(false);
	premium.setIsPremium(true);
	premium.setAccessProfile(true);
	
	// Initial Admin user
	UserApplication admin = new UserApplication();
	admin.setUsername("admin");
	admin.setPassword("admin");
	admin.setEmail("admin@creodron.pl");
	admin.setIsAdmin(true);
	admin.setIsPremium(true);
	admin.setAccessProfile(true);

	repository.add(regular);
	repository.add(premium);
	repository.add(admin);
	
	response.getWriter().println(""
			+ "<h1>Initalization Status - Accounts created successfully</h1>"
			+ "<h3>Administrator account: admin/admin</h3>"
			+ "<h3>Premium account: premium/premium</h3>"
			+ "<h3>Regular account: user/user</h3>"
			+ "<form action=\"login.jsp\" method=\"post\">"
			+ "<input type=\"submit\" value=\"OK\">"
			+ "</form>");

	}
}