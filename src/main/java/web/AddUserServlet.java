package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import repositories.DummyUserApplicationRepository;
import repositories.UserApplicationRepository;
import domain.UserApplication;


@WebServlet("/add")
public class AddUserServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	UserApplicationRepository repository;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		UserApplication user = retriveUserFromRequest(request);
		repository = new DummyUserApplicationRepository();
			
		session.setAttribute("conf", user);
		
		repository.add(user);
		response.sendRedirect("/success.jsp");
	}
  	
	private UserApplication retriveUserFromRequest (HttpServletRequest request){
		UserApplication result = new UserApplication();
	//	if ((request.getParameter("password")).equals(request.getParameter("confirm"))){
		result.setUsername(request.getParameter("username"));
		result.setPassword(request.getParameter("password"));
		result.setEmail(request.getParameter("email"));
		result.setIsPremium(false);
		result.setIsAdmin(false);
		result.setAccessProfile(true);
		
	//	}
		return result;
}
}