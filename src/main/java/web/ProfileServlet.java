package web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.UserApplication;
import repositories.DummyUserApplicationRepository;

@WebServlet("/profile")
	
	public class ProfileServlet extends HttpServlet {
	
		private static final long serialVersionUID = 1L;
		
		@Override 
		protected void doGet(HttpServletRequest request,HttpServletResponse response) 
		throws IOException,ServletException{
		    this.doPost(request,response);
		}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,  IOException{
			
		HttpSession session = request.getSession();
		UserApplication user = retriveApplicationFromRequest(request);
		List<UserApplication> repository = DummyUserApplicationRepository.getMyList();
		
		response.setContentType("text/html");
		if(session.getAttribute("permission") == null){
		    session.setAttribute("permission", "anonymous");
		    } 
			
		for(int i = 0;i<repository.size();i++){
		if(user.getUsername().equals(repository.get(i).getUsername())){
			response.getWriter().println("<h3>Your profile:</h3>"+
					"Username:" +
					repository.get(i).getUsername()+"</br>"+
					"E-mail:"+
					repository.get(i).getEmail()+"</br>"+
					"Password:"+
					repository.get(i).getPassword()+"</br>"+
					"Premium User:" +
					repository.get(i).getIsPremium()+"</br>"
					); 
			if(repository.get(i).getIsPremium()==true){
				response.getWriter().println(
						"<a href='/premium.jsp'>Premium content</a></br>");

		}
			if(repository.get(i).getIsAdmin()==true){
				response.getWriter().println(
						"<a href='/userslist'>Users List</a></br>");
				response.getWriter().println(
						"<a href='/managepermissions.jsp'>Manage permissions</a>");
		}
		}
		}
	}

	private UserApplication retriveApplicationFromRequest (HttpServletRequest request){
		UserApplication result = new UserApplication();
		result.setUsername(request.getParameter("username"));

		return result;
	}
}