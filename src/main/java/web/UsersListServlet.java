package web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.UserApplication;
import repositories.DummyUserApplicationRepository;

@WebServlet("/userslist")

public class UsersListServlet extends HttpServlet {
	
		private static final long serialVersionUID = 1L;
		
	 
		    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		        doPost(request,response);
		    }



	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
			
		List<UserApplication> repository = DummyUserApplicationRepository.getMyList();
		
		response.setContentType("text/html");
		
		response.getWriter().println(
				"<h3>Table below show all users registered in system</h3>"
				+ "<table border=\"1\">"
				+ "<tr><th>Id</th><th>Username</th><th>Password</th><th>E-mail</th><th>Premium</th></tr>");			
		
		for(int i = 0;i<repository.size();i++){
			response.getWriter().println(
					"<tr>"
					+ "<td>" + i + "</td>"
					+ "<td>" + repository.get(i).getUsername() + "</td>"
					+ "<td>" + repository.get(i).getPassword() + "</td>"
					+ "<td>" + repository.get(i).getEmail() + "</td>"
					+ "<td>" + repository.get(i).getIsPremium() + "</td>"
					+ "</tr>"
					); 
		
			}
		
		response.getWriter().println(
				"</table>");
		
		}	
	}