<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JAZ - LAB 2 - Welcome page</title>
</head>
<body>
<h1>Welcome</h1>
<h3><a href="login.jsp">Login page</a> - for registered users</h3>
<h3><a href="register.jsp">Register account</a> - for new users</h3>
<br/><br/>
<h5>If this is first time run - You can Initialize default users by clicking <a href="/userinit">here</a>.</h5>
</body>
</html>